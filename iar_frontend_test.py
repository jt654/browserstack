import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By

desired_cap = {
 'browserName': 'IE',
 'browser_version': '11.0',
 'os': 'Windows',
 'os_version': '10',
 'resolution': '1024x768',
 'name': 'Bstack-[Python] Sample Test'
}

desired_cap = {
 'browserName': 'Edge',
 'browser_version': '18.0',
 'os': 'Windows',
 'os_version': '10',
 'resolution': '1024x768',
 'name': 'Bstack-[Python] Sample Test' 
}

desired_cap = {
 'browserName': 'Firefox',
 'browser_version': '67.0',
 'os': 'Windows',
 'os_version': '10',
 'resolution': '1024x768',
 'name': 'Bstack-[Python] Sample Test'
}

desired_cap = {
 'browserName': 'Chrome',
 'browser_version': '74.0',
 'os': 'Windows',
 'os_version': '10',
 'resolution': '1024x768',
 'name': 'Bstack-[Python] Sample Test'
}

driver = webdriver.Remote(
    command_executor=os.environ['SERVER'],
    desired_capabilities=desired_cap)

driver.get('https://iar-test.gcloud.automation.uis.cam.ac.uk/')
if not "Information Asset Register" in driver.title:
    raise Exception("Unable to load Information Asset Register!")
elem = driver.find_element_by_css_selector("#root > div > div > div.jss6 > button").click()

driver.switch_to_window(driver.window_handles[1])
 
driver.find_element_by_id("userid").send_keys(os.environ['USERID'])
driver.find_element_by_id ("pwd").send_keys(os.environ['PASSWORD'])
driver.find_element_by_name("submit").click()

driver.quit()