# Automated Selenium Testing - BrowserStack (Asset Register)

This repository contains information about how to do automated testing using Selenium in order to test different desktop browsers with BrowserStack.

## Documentation 

The project has detailed documentation on how install, register and how to initiate a GitLab Runner in order to run the script for desktop browsers.

## Tools

*  Selenium Webdriver

## Requirements

The package required in order to run this project is the following:

*  Selenium

## Gitlab GitLab CI/CD Environment Variables Set Up

Enter the environment variables that are applied to environments via the runner:

``` bash
Please enter the gitlab-ci SERVER variable for this runner
xxx
```

``` bash
Please enter the gitlab-ci USERID variable for this runner
xxx
```

``` bash
Please enter the gitlab-ci PASSWORD variable for this runner
xxx
```

## GitLab CI/CD Runner Installation

Download the binary for you system:
``` bash
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
```
Give it permissions to execute:
``` bash
sudo chmod +x /usr/local/bin/gitlab-runner
```

## GitLab CI/CD Runner Registration
Run the following command:
``` bash
gitlab-runner register
``` 

Enter your GitLab instance URL:
``` bash
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
https://gitlab.com
```

Enter the token you obtained to register the Runner:
``` bash
Please enter the gitlab-ci token for this runner
xxx
``` 

## GitLab CI/CD Runner Start 
This command starts the GitLab Runner service.
``` bash
gitlab-runner start
``` 

## Test
Navigate to CI/CD > Pipelines and click on "Run Pipeline" to trigger the pipeline in case it has not run automatically.

Check that the status of the test is PASSED.

Navigate to Browserstack in order to check that the tests have been COMPLETED using the various OS and Broswers/Devices.

Browsers and Devices tested are the following:

- Android – Google Pixel 3
- Iphone XS
- Chrome – Windows 10
- Firefox – Windows 10
- Edge – Windows 10
- Safari – OS X Mojave
- Chrome – OS X Mojave
- Firefox – OS X Mojave
